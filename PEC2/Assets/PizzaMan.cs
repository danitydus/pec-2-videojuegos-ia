using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using TMPro;
public class PizzaMan : Agent
{
    Rigidbody rBody;
    public Transform Target;
    public float moveSpeed;
    public Transform fountain;
    public Transform fountainTarget;
    public float distanceToTarget;

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
    distanceToTarget = Vector3.Distance(Target.localPosition, fountainTarget.localPosition);
        if (distanceToTarget < 5f)
        {
            EndEpisode();
        }
        transform.LookAt(Target);

    }
    public override void OnEpisodeBegin()
    {


        // Move the target to a new spot
  
        Target.localPosition = new Vector3(Random.Range(-1f,+24f),
                                           1.7f, Random.Range(-4f, +21f));
        this.transform.localPosition = new Vector3(Random.Range(-1f, +24f),
                                           1.7f, Random.Range(-4f, +21f));

    }
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Vector3.Distance(Target.localPosition, this.transform.localPosition));
        sensor.AddObservation((Target.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(Vector3.Distance(fountain.localPosition, this.transform.localPosition));
        sensor.AddObservation((fountain.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(this.transform.forward);


        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];

        
        transform.position += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1f);
            this.transform.localPosition = new Vector3(Random.Range(-1f, +24f),
                                           1.7f, Random.Range(-4f, +21f));
            FindObjectOfType<camManager>().pizzasTotal = 0;
            EndEpisode();
        }
    }
    // Rewards
    private void OnTriggerStay(Collider Other)
    {
        // Reached target
        if (Other.tag == "PizzaTarget")
        {
            SetReward(+1f);
            FindObjectOfType<camManager>().pizzasTotal += 1;
             EndEpisode();
        }else if(Other.tag == "Valla")
        {
            SetReward(-1f);

            FindObjectOfType<camManager>().pizzasTotal = 0;
            EndEpisode();
        }else if(Other.tag == "Arbol")
        {
            FindObjectOfType<camManager>().pizzasTotal = 0;
            SetReward(-0.05f);
            EndEpisode();

        }
        else if(Other.tag == "Banco")
        {
            SetReward(-0.05f);
            FindObjectOfType<camManager>().pizzasTotal = 0;
            EndEpisode();

        }
        else if(Other.tag == "Fountain")
        {
            SetReward(-0.05f);
            FindObjectOfType<camManager>().pizzasTotal = 0;
            EndEpisode();

        }

    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

}
