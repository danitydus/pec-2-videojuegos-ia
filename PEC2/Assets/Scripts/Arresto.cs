using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BBUnity.Actions
{
    /// Acci�n que petmite sumar una multa al contador del policia cada vez que atrapa al ladr�n.
    [Action("Arresto")]
    [Help("")]
    public class Arresto : GOAction
    {
        private bool canSteal;

        // Cuando el policia entra en contacto con el ladr�n se suma una multa al contador.
        public override void OnStart()
        {
            gameObject.GetComponent<arrestoManager>().arrestoRobber();

        }
        public override TaskStatus OnUpdate()
        {
            return TaskStatus.RUNNING;
        }

    }
}
