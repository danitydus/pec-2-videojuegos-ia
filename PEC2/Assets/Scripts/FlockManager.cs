using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{
    public static FlockManager FM;
    public GameObject abejaPrefab;
    public int numAbejas = 20;
    public GameObject[] todasAbejas;
    public Vector3 limits = new Vector3(-5.49884224f, -0.670855641f, -0.387132645f);
    public Vector3 goalPos = Vector3.zero;


    [Header("Abejas Settings")]
    [Range(0.0f, 5.0f)]
    public float minSpeed;
    [Range(0.0f, 5.0f)]
    public float maxSpeed; 
    [Range(1.0f, 10.0f)]
    public float neighbourDistance; 
    [Range(1.0f, 5.0f)]
    public float rotationSpeed;
    // Se inicializan todas las abejas alrededor del panel.
    void Start()
    {
        todasAbejas = new GameObject[numAbejas];
        for(int i = 0; i < numAbejas; i++)
        {
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-limits.x, limits.x),
                Random.Range(-limits.y, limits.y),
                Random.Range(-limits.z, limits.z));
            todasAbejas[i] = Instantiate(abejaPrefab, pos, Quaternion.identity);
        }
        FM = this;
        goalPos = this.transform.position;
    }
    // Se determina el objetivo de movimiento de las abejas.
    void Update()
    {
        if (Random.Range(0, 100) < 2)
        {
            goalPos = this.transform.position + new Vector3(Random.Range(-limits.x, limits.x),
                Random.Range(-limits.y, limits.y),
                Random.Range(-limits.z, limits.z));
        }
    }
}
