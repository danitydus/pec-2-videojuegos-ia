using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class GhostPatrol : MonoBehaviour
{
    // Las variables que necesita el agente fantasma: las coordenadas de posici�n que
    // indican el destino del fantasma, y el agente runner que le seguir�.
    public Vector3 destino;
    public GameObject runner;
    public bool stoped;
    // La velocidad del agente fantasma tendr� siempre un valor aleatorio entre unos
    // m�rgenes definidos, al igual que su destino inicial, que siempre estar� dentro
    // de los l�mites del escenario.
    void Awake()
    {
        stoped = false;
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.isStopped = false;
        agente.speed = Random.Range(0.1f, 1);
        destino = new Vector3(Random.Range(2f, -25f), -16.7f, Random.Range(-13f, 12f));

    }
    // Durante todo momento el destino del agente fantasma ser� el equivalente al de la
    // variable destino. Esta cambiar� por otro valor aleatorio (dentro de
    // los m�rgenes del escenario) siempre que el agente haya llegado a su destino.
    // Adem�s, cuando el fantasma se distancie demasiado del runner se detendr� hasta
    // que la distancia vuelva a reducirse.
    void Update()
    {
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.destination = destino;

      
       if (agente.isStopped == false)
        {
            stoped = false;
        }
        else
        {
            stoped = true;
        }
    }
}
