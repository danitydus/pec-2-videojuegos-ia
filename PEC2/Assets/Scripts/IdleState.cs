using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public class IdleState : OldManIAState
{
    // La variable que indica la posici�n del banco que pasar� a ser la del agente anciano.
    public Vector3 bancoPosition;
    // El agente anciano.
    oldManAI myOldMan;
    // Inicializa el agente anciano.
    public IdleState(oldManAI oldman)
    {
        myOldMan = oldman;
    }
    void Start()  {}
    // Mientras el agente est� en el estado idle, se detendr� y el booleano que permite volver
    // a entrar en el estado idle tendr� valor negativo para evitar problemas en el cambio de estado.
    public void UpdateState()
    {
        myOldMan.canIdle = false;
        myOldMan.agent.isStopped = true;
    }
    // El estado actual cambia a wander.
    public void GoToWanderState()
    {
        myOldMan.currentState = myOldMan.wanderState;
    }
    public void GoToIdleState() {}
    public void OnTriggerEnter(Collider col) {}
    // Si el collider entrante es el banco, el agente anciano tomar� la posici�n del banco simulando la
    // acci�n de sentarse.
    public void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Banco")
        {
            bancoPosition = new Vector3 (col.transform.position.x, col.transform.position.y + 10, col.transform.position.z);
            myOldMan.gameObject.transform.position = bancoPosition;
        }
    }
    public void OnTriggerExit(Collider col) {}
}
