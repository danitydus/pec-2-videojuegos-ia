using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public interface OldManIAState
{
    // Las diferentes acciones que implementaran los estados posibles del agente anciano, wander e idle.
     void UpdateState();
     void GoToWanderState();
     void GoToIdleState();
     void OnTriggerEnter(Collider col);
     void OnTriggerStay(Collider col);
     void OnTriggerExit(Collider col);
}
