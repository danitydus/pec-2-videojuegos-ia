using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public class WanderState : OldManIAState
{
    // Las variables que necesita el estado wander; el vector de posici�n destino, y el booleano que permite
    // pasar al estado idle canIdle.
    public Vector3 destino;
    public bool canIdle;
    // El agente anciano.
    oldManAI myOldMan;
    // Inicializa el agente anciano.
    public WanderState(oldManAI oldman)
    {
        myOldMan = oldman;
    }
    public void Start(){
       
    }
    // En todo momento el agente anciano est� en movimiento y se comprueba si ha llegado ya a su destino.
    // Si es as�, el valor de la variable destino cambia por otro valor aleatorio dentro de los m�rgenes
    // del escenario.
    public void UpdateState()
    {
        myOldMan.agent.isStopped = false;
        myOldMan.agent.destination = destino;
        if (myOldMan.agent.remainingDistance <= myOldMan.agent.stoppingDistance)
        {
            destino = new Vector3(Random.Range(2, -23), -16.7f, Random.Range(7.5f, 34));
        }
    }
    public void GoToWanderState(){}
    // El estado actual cambia a idle.
    public void GoToIdleState()
    {
        myOldMan.currentState = myOldMan.idleState;
    }
    // Si el agente anciano entra en contacto con un banco y el valor del booleano canIdle es positivo
    // se inicia la acci�n descansar que lleva al agente al estado Idle durante unos segundos.
    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Banco" && myOldMan.canIdle == true) {
            myOldMan.canIdle = false;
            myOldMan.Descansar();
            GoToIdleState();
        }
    }
    public void OnTriggerStay(Collider col) {}
    public void OnTriggerExit(Collider col) {}
}
