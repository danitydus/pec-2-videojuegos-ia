using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class camManager : MonoBehaviour
{

    public GameObject camera;
    public int viewCam;
    public TextMeshProUGUI coinsCountText;
    public TextMeshProUGUI comidasCompletasCount;
    public bool canAddCoin;
    public bool isStealing;
    public int coinsCount;
    public int pizzasTotal;
    public int comidasCompletas;
    public TextMeshProUGUI pizzasCount;

    // Start is called before the first frame update
    void Start()
    {
        viewCam = 1;
        camera.transform.position = new Vector3(0.200000003f, 13.2200003f, -15.8000002f);
        camera.transform.eulerAngles = new Vector3(39.5394402f, 0, 0);
        canAddCoin = true;
        isStealing = false;
    }

    // Update is called once per frame
    void Update()
    {
        pizzasCount.SetText("" + pizzasTotal);
        comidasCompletasCount.SetText("" + comidasCompletas);

    }

    public void switchCam()
    {
        if ( viewCam == 1)
        {
            viewCam = 2;
            camera.transform.position = new Vector3(-20.5f, 13.2200003f, 3.5999999f);
            camera.transform.eulerAngles = new Vector3(39.5390015f, 90, 0);
        }else if (viewCam == 2)
        {
            viewCam = 3;
            camera.transform.position = new Vector3(-0.200000003f, 13.2200003f, 25.1000004f);
            camera.transform.eulerAngles = new Vector3(39.5390091f, 177.20697f, -1.32850082e-05f);
        
          }else if (viewCam == 3)
        {
            viewCam = 4;
            camera.transform.position = new Vector3(-0.200000003f, 18.6000004f, 24.1000004f);
            camera.transform.eulerAngles = new Vector3(51.4374466f, 176.5439f, 359.075195f);
        }else if (viewCam == 4)
        {
            viewCam = 1;
            camera.transform.position = new Vector3(0.200000003f, 13.2200003f, -15.8000002f);
            camera.transform.eulerAngles = new Vector3(39.5394402f, 0, 0);
        }

    }

    public void addCoin()
    {
        StartCoroutine(stealRoutine());
        coinsCount += 1;
        coinsCountText.SetText("" + coinsCount);

    }

    IEnumerator stealRoutine()
    {
        canAddCoin = false;
        isStealing = true;
        yield return new WaitForSeconds(2);
        isStealing = false;
        canAddCoin = true;
    }
}
