using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public class oldManAI : MonoBehaviour
{
   // Los dos estados que puede tener el agente anciano, wander e idle.
    [HideInInspector] public WanderState wanderState;
    [HideInInspector] public IdleState idleState;
    // La interfaz que define el estado actual del agente anciano.
    [HideInInspector] public OldManIAState currentState;
    [HideInInspector] public UnityEngine.AI.NavMeshAgent agent;
    // Las dos variables que usar� el agente anciano en su movimiento, el vector de posiciones que
    // indica su destino y el booleano que indica si puede pasar� al estado idle o no.
    public Vector3 destino;
    public bool canIdle;
    // Al inicio se inicializan los dos estados posibles para el agente anciano y se determina que el primer
    // estado sea siempre el de wander. Adem�s, la velocidad del agente anciano ser� aleatoria entre un rango
    // num�rico bastante bajo para que su velocidad siempre sea menor a la de los runners.
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        wanderState = new WanderState(this);
        idleState = new IdleState(this);
        currentState = wanderState;
        agent.speed = Random.Range(0.1f, 0.5f);
        SwitchCanIdle();
    }
    // Ejecuta la acci�n UpdateState durante todo el rato y en cada uno de los dos estados.
    public void Update()
    {
        currentState.UpdateState();
    }
    // Acci�n que se llama desde los estados del agente para iniciar la co rutina switchCanIdle.
     public void SwitchCanIdle()
    {
        StartCoroutine(switchCanIdle());
    }
    // Co rutina que cambia el valor del booleano canIdle para permitir pasar o no al estado de idle.
    IEnumerator switchCanIdle()
    {
        canIdle = false;
        yield return new WaitForSeconds(6f);
        canIdle = true;
    }
    // Acci�n que se llama desde el estado wander para iniciar la co rutina descansar.
    public void Descansar()
    {
        StartCoroutine(descansar());
    }
    // Co rutina que consiste en que despu�s de 4 segundos en el estado idle (descanso), el agente anciano
    // vuelve al estado wander y el valor del booleano canIdle se vuelve hasta que la co rutina switchCanIdle
    // lo vuelva a modificar. Esto hace que el anciano no pueda volver a sentarse al mismo banco hasta pasados
    // unos segundos.
   IEnumerator descansar()
    {
        yield return new WaitForSeconds(Random.Range(3f, 5f));
        currentState = wanderState;
        canIdle = false;
        SwitchCanIdle();
    }
    // Acciones que se ejecutan en el momento en que el collider del banco entra, se mantiene o sale
    // del collider del agente anciano.
    void OnTriggerEnter(Collider col)
    {
        currentState.OnTriggerEnter(col);
    }
    void OnTriggerStay(Collider col)
    {
        currentState.OnTriggerStay(col);
    }
    void OnTriggerExit(Collider col)
    {
        currentState.OnTriggerExit(col);
    }
}
