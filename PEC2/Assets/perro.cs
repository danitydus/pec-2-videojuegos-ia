using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using TMPro;
public class perro : Agent
{
    Rigidbody rBody;
    public int comidasCompletas;
    public bool canEat;
    public bool canDrink;

    public Transform comida1;
    public Transform agua1;

    public GameObject comida;
    public GameObject agua;




    public float moveSpeed;
    void Start()
    {
        agua.SetActive(false);
        comida.SetActive(true);
        rBody = GetComponent<Rigidbody>();
        canEat = true;
    }

    void Update()
    {

    }
    public override void OnEpisodeBegin()
    {

        // Move the target to a new spot
       

    }
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Vector3.Distance(agua1.localPosition, this.transform.localPosition));
        sensor.AddObservation((agua1.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(Vector3.Distance(comida1.localPosition, this.transform.localPosition));
        sensor.AddObservation((comida1.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(this.transform.forward);

        

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];


        transform.position += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
        if (this.transform.localPosition.y < 14)
        {
            SetReward(-1f);
            FindObjectOfType<camManager>().comidasCompletas = 0;
            this.transform.localPosition = new Vector3(Random.Range(-18f, +6f),
                                           15.7f, Random.Range(-11f, +12f));
            EndEpisode();
        }
    }
    private void OnTriggerStay(Collider Other)
    {
        if (Other.tag == "Comida")
        {
           
                FindObjectOfType<camManager>().comidasCompletas += 1;
                comida.SetActive(false);
                agua.SetActive(true);
                SetReward(+1f);
                EndEpisode();
            }

         if (Other.tag == "Agua")
        {
            agua.SetActive(false);
            comida.SetActive(true);
            SetReward(+1f);
            FindObjectOfType<camManager>().comidasCompletas += 1;
            EndEpisode();


        }
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

}
